const mysql = require('mysql2')

const Pool = mysql.createPool({
  connectionLimit: 20,
  user: 'root',
  password: 'manager',
  database: 'cardb',
  port: 3306,

  waitForConnections: true,
  queueLimit: 0,
})

module.exports = {
  Pool,
}
