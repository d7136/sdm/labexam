const express = require('express')

const app = express()

app.listen(4000, '127.0.0.1', () => {
  console.log('server started listening on port 4000...')
})
